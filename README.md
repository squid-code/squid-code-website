# Squid Code

- Install NodeJS

- Install `yarn` by doing `npm install -g yarn`

- Install dependencies by running `yarn`

- To run the development mode do `npm start`

- It should automatically reload when you make changes to the JavaScript

- Create a branch by doing `git checkout -b your-branch-name`

- Push your changes to your branch `git push origin your-branch-name`

- Create a Merge Request in GitLab

- After your MR is reviewed you can merge your changes

- They will be available on the Netlify hosted production site
