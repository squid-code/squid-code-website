import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';

import { RouterProvider, Route } from 'react-router5'
import createRouter from 'router5';
import browserPlugin from 'router5-plugin-browser';

const routes = [
  { name: 'home', path: '/' },
]

const router = createRouter(routes);

router.usePlugin(browserPlugin());
router.start();


ReactDOM.render(
  <RouterProvider router={router}>
    <Route>
      {routeProps => <App route={routeProps.route} />}
    </Route>
  </RouterProvider>,
  document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
