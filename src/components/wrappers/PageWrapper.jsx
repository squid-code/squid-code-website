import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'reactstrap';
import styled from 'styled-components';

const StyledContainer = styled(Container)`
  &&& {
    min-height: 90vh;
    width: 100vw;
    @media (min-width: 500px) {
      padding: ${({verticalPadding}) => verticalPadding ? '5em' : 0} ${({horizontalPadding}) => horizontalPadding ? '15vw' : 0};
    }
  }
`

const PageWrapper = ({children, ...props}) => (
  <StyledContainer {...props} >
    {children}
  </StyledContainer>
);

PageWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  horizontalPadding: PropTypes.bool,
  verticalPadding: PropTypes.bool,
};

PageWrapper.defaultProps = {
  horizontalPadding: false,
  verticalPadding: false,
};

export default PageWrapper;