import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

const StyledText = styled.p`
  font-size: ${({size}) => size * 8}px;
  padding: 24px 0;
`;

const SquidText = ({ children, ...props }) => <StyledText {...props}>{children}</StyledText>;

SquidText.propTypes = {
  size: PropTypes.number,
};

SquidText.defaultText = {
  size: 1,
};

SquidText.propTypes = {
  children: PropTypes.node.isRequired,
}

export default SquidText;