import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Shape = styled.div`
  -webkit-transform: skew(-30deg);
  -moz-transform: skew(-30deg);
  transform: skew(-30deg);
  background: #000;
  color:#fff;

  padding: 10px 20px;
`;

const Text = styled.div`
  margin: auto;
  -webkit-transform: skew(30deg);
  -moz-transform: skew(30deg);
  transform: skew(30deg);
  color:#fff;
  text-align: center;
`

const SquidShape = ({ children }) => (
  <Shape><Text>{children}</Text></Shape>
);

SquidShape.propTypes = {
  children: PropTypes.node.isRequired,
}

export default SquidShape;