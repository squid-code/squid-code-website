import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

const NavBar = () => {
  const [isOpen, setIsOpen] = useState(true);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">Squid Code</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="https://gitlab.com/squid-code">GitLab</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Projects
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem header>
                  Active
                </DropdownItem>
                <DropdownItem href="https://www.squidcode.io">
                  Squid Code
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem header>
                  In Progress
                </DropdownItem>
                <DropdownItem disabled>
                  Squid Push
                </DropdownItem>
                <DropdownItem href="https://toroban.com/">
                  Toroban
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Developer Blogs
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href="https://ayebear.com">
                  Ayebear
                </DropdownItem>
                <DropdownItem disabled>
                  Blaze
                </DropdownItem>
                <DropdownItem disabled>
                  NP
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <NavbarText>Software Engineers</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default NavBar;