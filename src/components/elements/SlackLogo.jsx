import React from 'react';
import PropTypes from 'prop-types';

const SlackLogo = ({workspace}) => (
  <img
    alt="Squid Code Slack Logo"
    src={`https://slack.${workspace}.com/badge.svg`}
  />
);

SlackLogo.propTypes = {
  workspace: PropTypes.string.isRequired,
};

export default SlackLogo;