import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Heading = styled.h3`
  padding: 24px 0;
`;

const SquidHeading = ({children}) => <Heading>{children}</Heading>;

SquidHeading.propTypes = {
  children: PropTypes.node.isRequired,
}

export default SquidHeading;