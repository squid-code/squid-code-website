import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledContainer = styled.div`
  height: 10vh;
  max-height: 10vh;
  min-height: 10vh;
  position: relative;
`;
const StyledFooter = styled.div`
  &&& {
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    text-align: center;
    top: 0;
  }

`;

const Footer = ({ children }) => (
  <StyledContainer fluid>
    <StyledFooter>
      {children}
    </StyledFooter>
  </StyledContainer>
);

Footer.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Footer;