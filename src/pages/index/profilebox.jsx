import React from 'react';
import PropTypes from 'prop-types';

import styled from 'styled-components';

const StyledBox = styled.div`
    margin: auto;
    border-style: solid;
    border-radius: 2px;
    border-width: 1px;
    width: 200px;

    text-align: center;

    color: white;
    background-color: black;
    margin-top: 5px;
    margin-bottom: 5px;

    padding-top: 5px;
    padding-bottom: 5px;
`;

const ProfileBox = ({ company, name }) => (
    <StyledBox>
        <p>{name && `${name} - `}{company}</p>
    </StyledBox>
);

ProfileBox.propTypes = {
    company: PropTypes.string.isRequired,
    name: PropTypes.string,
};

ProfileBox.defaultProps = {
    name: '',
};

export default ProfileBox;