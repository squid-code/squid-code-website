import React from 'react';
import { Col, Row } from 'reactstrap';

import Footer from '../../components/elements/Footer';
import PageWrapper from '../../components/wrappers/PageWrapper';
import SquidHeading from '../../components/elements/SquidHeading';
import SquidShape from '../../components/elements/SquidShape';
import SquidText from '../../components/elements/SquidText';

import styled from 'styled-components';
import './general.css'

import ProfileBox from './profilebox.jsx';

const HeaderRow = styled(Row)`
    padding: 5em 0 1em;
`;

const StyledA = styled.a`
    color: black;

    &:hover {
        background-color: black;
        border-color: black;
        border-style: solid;
        color: white;
        text-decoration: none;
    }
`;

const Index = () => (
    <>
    <PageWrapper horizontalPadding>
        <HeaderRow>
            <Col>
                <SquidShape>
                    <h1>Squid Code Group</h1>
                </SquidShape>
            </Col>
        </HeaderRow>
        <Row>
            <Col>
            <SquidText size={3}>
                Welcome to our website, we are a national group of developers with the
                goal to create first class, high performance applications.
            </SquidText>
            {/*<SlackLogo workspace="squid-code" />*/}
            <hr />
            </Col>
        </Row>
        <Row>
            <Col>
                <SquidHeading>
                    Our members have worked at several of the following companies
                </SquidHeading>
            </Col>
        </Row>
        <Row>
            <Col>
                <ProfileBox name="" company="Amazon" />
                <ProfileBox name="" company="Cloudreach" />
                <ProfileBox name="" company="Healytics" />
                <ProfileBox name="" company="Intel" />
            </Col>
            <Col>
                <ProfileBox name="" company="Capital One" />
                <ProfileBox name="" company="Lyft" />
                <ProfileBox name="" company="T Rowe Price" />
                <ProfileBox name="" company='ApplicationsOnline' />
            </Col>
        </Row>
    </PageWrapper>
    <Footer>
        <StyledA href="./terms.html">Terms and Conditions</StyledA> |
        <StyledA href="./privacy.html"> Privacy Policy</StyledA>
    </Footer>
    </>
);

export default Index;