import React from 'react';

import NavBar from './components/elements/NavBar';

import Index from './pages/index';

const App = ({ route }) => {
  let currentPage = (<h1>404 Not Found</h1>);
  switch (route.name) {
    case 'home':
      currentPage = (<Index />);
      break;
    default: 
      currentPage = (<Index />);
      break;
  }
  return (
    <>
      <NavBar />
      {currentPage}
    </>
  );
}

export default App;
